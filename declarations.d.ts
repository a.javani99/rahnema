declare namespace Express {
    export interface Request {
        user?: import('./src/model/profile/user').IUser
        admin?: import('./src/model/admin/admin').IAdmin
        token?: string
    }
    export interface Response {
        modifiedSend: any
    }
}
