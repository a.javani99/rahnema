import { Document } from 'mongoose'
interface CDocument extends Document {
    deleted: boolean
    apply(id?: string, ip?: string): void
}

export default CDocument
