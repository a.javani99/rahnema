export const teamMemberStatusTypes = ['pending', 'joined']
export const teamMemberRoleTypes = ['owner', 'biller', 'member']

import mongoose, { Document, Model } from 'mongoose'
import CDocument from '../../../CustomDocument'
import mainDB, { model } from '../../db/mongoose'
import { generalApply } from '../../utilities.js'
import { IAdmin } from '../admin/admin'
const Schema = mongoose.Schema

export interface IConstant extends CDocument {
    key: string
    values: string[]
}

interface ConstantModel extends Model<IConstant> {}

const constantSchema = new Schema<IConstant>({
    key: {
        type: String,
        unique: true,
        sparse: true,
    },
    values: [
        {
            type: String,
        },
    ],
})

constantSchema.methods.apply = async function (id: string, ip: string) {
    await generalApply(id, this, Constant, ip)
}

const Constant = model<IConstant, ConstantModel>('Constant', constantSchema)
export default Constant
