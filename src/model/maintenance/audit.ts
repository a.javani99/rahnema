import mongoose, { Document, Model, Types } from 'mongoose'
import mainDB, { model } from '../../db/mongoose'
const Schema = mongoose.Schema
import CDocument from '../../../CustomDocument.js'

export interface IAudit extends CDocument {
    type: string
    col: string
    docId: string
    change: object
    AdminId: Types.ObjectId
    UserId: Types.ObjectId
    TeamId: Types.ObjectId
    ip: string
}

interface AuditModel extends Model<IAudit> {}

const auditSchema = new mongoose.Schema<IAudit>(
    {
        type: String,
        col: String,
        docId: String,
        change: Object,
        AdminId: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
        },
        UserId: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
        TeamId: {
            type: Schema.Types.ObjectId,
            ref: 'Team',
        },
        ip: String,
    },
    { timestamps: true }
)

const Audit = model<IAudit, AuditModel>('Audit', auditSchema)
export default Audit
