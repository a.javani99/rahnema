import mongoose, { Document, Model, ObjectId, Types } from 'mongoose'
import CDocument from '../../../CustomDocument.js'
import mainDB, { model } from '../../db/mongoose'
import { generalApply } from '../../utilities.js'
const Schema = mongoose.Schema

export interface IAPI extends CDocument {
    route: string
    method: string
    routerFile: string
    validInputs: string[]
    requiredInputs: string[]
    foreignKeys: string[]
}

interface APIModel extends Model<IAPI> {
    findOrCreate(data: any): IAPI
}

const apiSchema = new Schema<IAPI>(
    {
        route: {
            type: String,
        },
        method: {
            type: String,
        },
        routerFile: {
            type: String,
        },
        validInputs: [String],
        requiredInputs: [String],
        foreignKeys: [String],
    },
    { timestamps: true }
)

apiSchema.statics.findOrCreate = async (data) => {
    let doc: any = await API.findOne({ route: data.route, method: data.method })
    if (doc) {
        doc.validInputs = data.validInputs
        doc.requiredInputs = data.requiredInputs
        doc.foreignKeys = data.foreignKeys
        await doc.save()
        return doc
    }
    doc = await API.create(data)
    return doc
}

apiSchema.methods.apply = async function (id: string, ip: string) {
    await generalApply(id, this, API, ip)
}

const API = model<IAPI, APIModel>('API', apiSchema)
export default API
