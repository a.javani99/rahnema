import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import mongoose, { Model, Types } from 'mongoose'
import CDocument from '../../../CustomDocument.js'
import { model } from '../../db/mongoose.js'
import { generalApply } from '../../utilities.js'
const Schema = mongoose.Schema
export interface IUser extends CDocument {
    username: string
    password: string
    sessions: { token: string; lastTimeUsed: number }[]
    generateToken(): string
}
interface UserModel extends Model<IUser> {
    findByCredentials(login: string, password: string): IUser
}

const userSchema = new mongoose.Schema<IUser>(
    {
        username: {
            type: String,
            unique: true,
            sparse: true,
        },
        password: {
            type: String,
        },
        sessions: [{ token: String, lastTimeUsed: Number }]
    },
    { timestamps: true }
)

userSchema.pre('save', async function (next) {
    const user = this
    if (user.isModified('password') && user.password) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

userSchema.statics.findByCredentials = async (login, password) => {
    try {
        const user = await User.findOne({
            $or: [{ username: login }, { email: login }, { phone: login }],
        })
        if (!user) {
            throw new Error('User not found')
        } else {
            const isPasswordCorrect = await bcrypt.compare(password.toString(), user.password)
            if (!isPasswordCorrect) {
                throw new Error('User not found')
            } else {
                return user
            }
        }
    } catch (e: any) {
        throw new Error('User not found')
    }
}

userSchema.methods.generateToken = async function () {
    try {
        if (!process.env.JWT_SECRET) throw new Error('no jwt secret found')
        const user = this
        const token = jwt.sign(
            {
                id: user.id.toString(),
            },
            process.env.JWT_SECRET
        )
        user.sessions = user.sessions.concat({ token, lastTimeUsed: Date.now() })
        await user.save()
        return token
    } catch (e) {
        console.log(e)
    }
}


userSchema.methods.toJSON = function () {
    const modelObject = this.toObject() as any
    delete modelObject.password
    return modelObject
}

userSchema.methods.apply = async function (id: string, ip: string) {
    await generalApply(id, this, User, ip)
}

const User = model<IUser, UserModel>('User', userSchema)
export default User
