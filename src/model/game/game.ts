import mongoose, { Document, Model, Types } from 'mongoose'
import mainDB, { model } from '../../db/mongoose'
const Schema = mongoose.Schema
import CDocument from '../../../CustomDocument.js'

export interface IGame extends CDocument {
    count: number
    creator: string
    letter: string
    letters: string[]
    players: {
        UserId: string
        answers: { answer: string, score: number }[]
        totalScore: number
    }[]
    status: string
}

interface GameModel extends Model<IGame> { }

const gameSchema = new mongoose.Schema<IGame>(
    {
        count: Number,
        creator: String,
        letter: String,
        letters: {
            type: [String],
            default: []
        },
        players: [{
            UserId: String,
            answers: [{
                answer: String,
                score: Number
            }],
            totalScore: Number
        }],
        status: {
            type: String,
            default: 'Open'
        }
    },
    { timestamps: true }
)

const Game = model<IGame, GameModel>('Game', gameSchema)
export default Game
