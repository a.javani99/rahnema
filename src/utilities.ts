import { Request } from 'express'
import { UploadedFile } from 'express-fileupload'
import fs from 'fs'
import mongoose, { Model } from 'mongoose'
import Admin from './model/admin/admin.js'
import Audit from './model/maintenance/audit.js'
import User from './model/profile/user.js'


export const generalApply = async (id: string, doc: any, Model: Model<any>, ip?: string) => {
    try {
        if (!mongoose.isValidObjectId(id)) id = '000000000000000000000000'
        const user = await User.findById(id)
        const admin = await Admin.findById(id)
        const modelObj = doc.toObject() as any
        const before = await Model.findById(doc.id)
        const result: any = {}
        if (before) {
            const beforeObj = before.toObject() as any
            for (let key of Object.keys(modelObj)) {
                if (typeof modelObj[key] == 'object') {
                    if (modelObj && modelObj[key] && beforeObj && beforeObj[key] && modelObj[key].toString() !== beforeObj[key].toString()) {
                        result[key] = { before: beforeObj[key], after: modelObj[key] }
                    }
                } else {
                    if (modelObj[key] !== beforeObj[key]) {
                        result[key] = { before: beforeObj[key], after: modelObj[key] }
                    }
                }
            }
            for (let key of Object.keys(beforeObj)) {
                if (typeof beforeObj[key] == 'object') {
                    if (modelObj && modelObj[key] && beforeObj && beforeObj[key] && modelObj[key].toString() !== beforeObj[key].toString()) {
                        result[key] = { before: beforeObj[key], after: modelObj[key] }
                    }
                } else {
                    if (modelObj[key] !== beforeObj[key]) {
                        result[key] = { before: beforeObj[key], after: modelObj[key] }
                    }
                }
            }
            if (Object.keys(result).length > 0) await Audit.create({ UserId: user?._id, AdminId: admin?._id, type: 'modify', col: Model.modelName, docId: doc.id, change: result, ip })
        } else {
            await Audit.create({ TeamId: doc.TeamId, UserId: user?._id, AdminId: admin?._id, type: 'create', col: Model.modelName, docId: doc.id, change: modelObj, ip })
        }
        console.log('Temp Log: ' + JSON.stringify({ TeamId: doc.TeamId, UserId: user?._id, AdminId: admin?._id, type: 'create', col: Model.modelName, docId: doc.id, change: modelObj, ip }))
        await doc.save()
    } catch (e) {}
}

export const genPassword = (passwordLength = 8) => {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var password = ''
    for (var i = 0; i <= passwordLength; i++) {
        var randomNumber = Math.floor(Math.random() * chars.length)
        password += chars.substring(randomNumber, randomNumber + 1)
    }
    return password
}

export const filter = (data: any, sample: any) => {
    const clone = JSON.parse(JSON.stringify(data))
    try {
        if (!(typeof clone == 'object')) return clone
        if (typeof clone == 'object' && typeof sample == 'string') return clone
        for (let key in clone) {
            if (!(key in sample)) {
                delete clone[key]
            } else {
                if (sample[key] == 'any') continue
                if (typeof clone[key] == 'object') {
                    if (Array.isArray(clone[key])) {
                        for (let i = 0; i < clone[key].length; i++) {
                            clone[key][i] = filter(clone[key][i], sample[key][0])
                        }
                    } else {
                        clone[key] = filter(clone[key], sample[key])
                    }
                }
            }
        }
        return clone
    } catch (e) {
        return clone
    }
}

export const findModel = (name: string) => {
    try {
        const files = listFiles([], process.cwd() + '/dist/src/model')
        if (!files) return null
        for (const file of files) {
            if (!file.path.endsWith('.map')) {
                try {
                    const Model = require(file.path).default
                    if (Model.modelName.toLowerCase() == name.toLowerCase()) {
                        return { Model, file }
                    }
                } catch (e) {}
            }
        }
        return null
    } catch (e) {
        return null
    }
}

export const listFiles = (result: { name: string; path: string }[], dir: string) => {
    try {
        if (!result) result = []
        const files = fs.readdirSync(dir)
        for (let i = 0; i < files.length; i++) {
            if (fs.lstatSync(dir + '/' + files[i]).isDirectory()) {
                const recursive = listFiles(result, dir + '/' + files[i])
                if (recursive) result = recursive
            } else {
                result.push({ name: files[i], path: dir + '/' + files[i] })
            }
        }
        return result
    } catch (e) {
        if (e instanceof Error) console.log(e.stack)
    }
}

export const genQueryObjs = (req: Request, Model: Model<any>) => {
    const queryKeys = Object.keys(req.query)
    const staticFields = Object.keys(Model.schema.paths)
    const where: any = {}
    for (let i = 0; i < queryKeys.length; i++) {
        let key = queryKeys[i]
        let whereKey = key
        let mode = 0
        if (key.includes('contain')) {
            mode = 1
            whereKey = key.substr(7)
        } else if (key.includes('startsWith')) {
            mode = 2
            whereKey = key.substr(10)
        } else if (key.includes('endsWith')) {
            mode = 3
            whereKey = key.substr(8)
        } else if (key.includes('max')) {
            mode = 4
            whereKey = key.substr(3)
        } else if (key.includes('min')) {
            mode = 5
            whereKey = key.substr(3)
        }
        if (!staticFields.includes(whereKey)) {
            continue
        }
        let $or: any = []
        switch (mode) {
            case 0:
                if (!where[whereKey]) where[whereKey] = {}
                if (isNaN(Number(req.query[key]))) {
                    where[whereKey].$eq = req.query[key]
                } else {
                    where[whereKey].$in = [req.query[key], parseFloat(req.query[key] as string)]
                }
                break
            case 1:
                if (!where[whereKey]) where[whereKey] = {}
                where[whereKey].$regex = req.query[key]
                where[whereKey].$options = 'i'
                break
            case 2:
                if (!where[whereKey]) where[whereKey] = {}
                where[whereKey].$regex = '^' + req.query[key]
                where[whereKey].$options = 'i'
                break
            case 3:
                if (!where[whereKey]) where[whereKey] = {}
                where[whereKey].$regex = req.query[key] + '$'
                where[whereKey].$options = 'i'
                break
            case 4:
                if (!where.$and) where.$and = []
                $or = [
                    {
                        [whereKey]: { $lte: req.query[key] },
                    },
                ]
                if (!isNaN(Number(req.query[key]))) {
                    $or.push({
                        [whereKey]: { $lte: parseFloat(req.query[key] as string) },
                    })
                }
                where.$and.push({ $or })
                break
            case 5:
                if (!where.$and) where.$and = []
                $or = [
                    {
                        [whereKey]: { $gte: req.query[key] },
                    },
                ]
                if (!isNaN(Number(req.query[key]))) {
                    $or.push({
                        [whereKey]: { $gte: parseFloat(req.query[key] as string) },
                    })
                }
                where.$and.push({ $or })
                break
        }
    }
    let order: any = { _id: 1 }
    let sortField: string | null = null
    let doubleQuery = false
    if (req.query.sort) {
        doubleQuery = true
        sortField = req.query.sort as string
        order = {}
        order[sortField] = 1
        if ('order' in req.query && req.query.order === 'DESC') {
            order[sortField] = -1
            doubleQuery = false
        }
        order._id = 1
    }
    let page = 1
    if (req.query.page) {
        page = parseInt(req.query.page as string)
    }
    let pageSize = 25
    if (req.query.pageSize && !isNaN(Number(req.query.pageSize as string))) {
        pageSize = parseInt(req.query.pageSize as string)
    }
    return { where, order, doubleQuery, page, pageSize, sortField } as options
}

interface options {
    where: any
    order: any
    doubleQuery: boolean
    page: number
    pageSize: number
    sortField: string
}

export const query = async (Model: Model<any>, populations: string[] | { field: string; selection: string | object }[], options: options) => {
    try {
        let { where, order, doubleQuery, page, pageSize, sortField } = options
        let result: any = []
        result = Model.find(where)
        for (let p of populations) {
            if (typeof p == 'string') {
                result = result.populate(p)
            } else {
                result = result.populate(p.field, p.selection)
            }
        }
        result = result
            .sort(order)
            .skip((page - 1) * pageSize)
            .limit(pageSize)
        result = await result
        if (!doubleQuery || true) return result
    } catch (e) {
        if (e instanceof Error) console.log(e.message)
    }
}

export const genCode = (length: number) => {
    var result = ''
    var characters = '0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
}

export const saveFile = async (file: UploadedFile, path: string, privateFile = false) => {
    return new Promise<string>((resolve, reject) => {
        let basePath = process.cwd() + '/public/' + path
        if (privateFile) basePath = process.cwd() + '/private/' + path
        if (!fs.existsSync(basePath)) {
            fs.mkdirSync(basePath, { recursive: true })
        }
        const fileName = Date.now() + '-' + file.name
        file.mv(basePath + '/' + fileName, (err: any) => {
            if (err) {
                reject(err)
            } else {
                resolve('/' + path + '/' + fileName)
            }
        })
    })
}
