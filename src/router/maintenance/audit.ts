import express from 'express'
import { auth } from '../../middleware/authentication'
import Audit from '../../model/maintenance/audit'
import { genQueryObjs, query } from '../../utilities'
const router = express.Router()

router.get('/audit', auth(3), async (req, res) => {
    try {
        const options = genQueryObjs(req, Audit)
        const result = await query(Audit, [], options)
        const total = await Audit.count(options.where)
        res.send({ result, total })
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

export default router
