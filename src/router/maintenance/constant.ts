import express from 'express'
import { UploadedFile } from 'express-fileupload'
import { auth } from '../../middleware/authentication.js'
import Constant from '../../model/maintenance/constant.js'
import { genQueryObjs, query, saveFile } from '../../utilities.js'

const router = express.Router()

router.post('/constant', auth(1), async (req, res) => {
    try {
        if (!req.admin) return res.status(500).send({ error: 'admin not found' })
        let constant = new Constant(req.body)
        await constant.apply(req.admin.id)
        res.modifiedSend(constant)
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

router.get('/constant', async (req, res) => {
    try {
        const options = genQueryObjs(req, Constant)
        const total = await Constant.count(options.where)
        const result = await query(Constant, [], options)
        res.send({ result, total })
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

router.get('/constant/:key', async (req, res) => {
    try {
        const constant = await Constant.findOne({ key: req.params.key })
        if (!constant) {
            res.status(404).send({ msg: 'not found' })
            return
        }
        res.modifiedSend(constant)
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

router.get('/constant/:id', async (req, res) => {
    try {
        const constant = await Constant.findById(req.params.id)
        if (!constant) {
            res.status(404).send({ msg: 'not found' })
            return
        }
        res.modifiedSend(constant)
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

router.patch('/constant/:id', auth(1), async (req, res) => {
    try {
        if (!req.admin) return res.status(500).send({ error: 'admin not found' })
        const constant = await Constant.findById(req.params.id)
        if (!constant) {
            res.status(404).send({ msg: 'not found' })
            return
        }
        constant.set(req.body)
        if (req.files && req.files.file) {
            const path = await saveFile(req.files.file as UploadedFile, 'public/default')
            constant.values = [path]
        }
        await constant.apply(req.admin.id)
        res.modifiedSend(constant)
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

router.delete('/constant/:id', auth(1), async (req, res) => {
    try {
        if (!req.admin) return res.status(500).send({ error: 'admin not found' })
        let constant = await Constant.findById(req.params.id)
        if (!constant) {
            res.status(404).send({ msg: 'not found' })
            return
        }
        if (constant.key == 'avatar' || constant.key == 'teamAvatar' || constant.key == 'projectAvatar') {
            res.status(404).send({ msg: "default avatar can't be deleted" })
            return
        }
        constant.deleted = true
        await constant.apply(req.admin.id)
        res.status(200).send({ msg: 'done' })
    } catch (e) {
        if (e instanceof Error) res.status(400).send({ error: e.message })
    }
})

export default router
