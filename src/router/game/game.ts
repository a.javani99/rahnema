import express from 'express'
import getRedis from '../../db/redis'
import { auth } from '../../middleware/authentication'
import Game, { IGame } from '../../model/game/game.js'
const redis = getRedis()
const router = express.Router()

router.post('/game/new', auth(2), async (req, res) => {
    try {
        const count = req.body.count
        const letter = req.body.letter
        const players = req.body.players
        // better validation with joi, obviously
        if (!count || !letter) return res.status(400).send({ error: 'count or letter is missing' })
        if (isNaN(count)) return res.status(400).send({ error: 'count should be a number' })
        if (letter.length != 1) return res.status(400).send({ error: 'letter should have length of one' })


        const game = await Game.create({ count, letter, letters: [letter], players: [], creator: req.user?.id })
        for (let player of players) game.players = game.players.concat({ UserId: player, answers: [], totalScore: 0 })
        game.markModified('players')
        await game.save()
        res.send(game)
    } catch (e: any) {
        res.status(500).send({ error: e.message })
    }
})

router.get('/game/:id', auth(2), async (req, res) => {
    try {
        const game = await Game.findById(req.params.id)
        if (!game || !game.players.map(a => a.UserId).includes(req.user?.id)) return res.status(404).send({ error: 'game not found' })
        res.send(game)
    } catch (e: any) {
        res.status(400).send({ error: e.message });
    }
})

router.post('/game/:id/answer', auth(2), async (req, res) => {
    try {
        const game = await Game.findById(req.params.id)
        if (!game || !game.players.map(a => a.UserId).includes(req.user?.id)) return res.status(404).send({ error: 'game not found' })
        const answer = req.body.answer || ''
        const id = req.user?.id
        const player = game.players.find(a => a.UserId == id)
        if (!player) return res.status(500).send({ error: 'server error' })
        if (player.answers.length == game.letters.length) return res.status(400).send({ error: 'you already answered' })
        player.answers = player.answers.concat({ answer, score: 0 })
        const answerMap: any = {}
        const index = game.letters.length
        for (let player of game.players) {
            if (player.answers[index]) {
                if (answerMap[player.answers[index].answer]) answerMap[player.answers[index].answer] = 5
                else answerMap[player.answers[index].answer] = 10
            }
        }
        for (let player of game.players) {
            if (player.answers[index]) {
                player.answers[index].score = answerMap[player.answers[index].answer]
                if (!player.answers[index].answer.startsWith(game.letter) || player.answers[index].answer == '') player.answers[index].score = 0
            }
        }
        game.markModified('players')
        await game.save()
        res.send(game)
    } catch (e: any) {
        res.status(400).send({ error: e.message });
    }
})

router.post('/game/:id/letter', auth(2), async (req, res) => {
    try {
        const game = await Game.findById(req.params.id)
        if (!game) return res.status(404).send({ error: 'game not found' })
        if (req.user?.id != game.creator) return res.status(403).send({ error: 'forbidden' })
        if (game.players.findIndex(a=>a.answers.length!=game.letters.length) != -1) return res.status(403).send({ error: 'all players should answer first' })
        const letter = req.body.letter
        if (letter.length != 1) return res.status(400).send({ error: 'letter should have length of one' })
        game.letter = letter
        game.letters = game.letters.concat(letter)
        await game.save()
        res.send(game)
    } catch (e: any) {
        res.status(400).send({ error: e.message });
    }
})

router.post('/game/:id/close', auth(2), async (req, res) => {
    try {
        const game = await Game.findById(req.params.id)
        if (!game) return res.status(404).send({ error: 'game not found' })
        if (req.user?.id != game.creator) return res.status(403).send({ error: 'forbidden' })
        game.status = 'Closed'
        await game.save()
        res.send(game)
    } catch (e: any) {
        res.status(400).send({ error: e.message });
    }
})


export default router
