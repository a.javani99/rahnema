import express from 'express'
import { auth } from '../../middleware/authentication'
import API, { IAPI } from '../../model/admin/api'
const router = express.Router()

router.get('/api', auth(1), async (req, res) => {
    res.modifiedSend(await API.find())
})

router.get('/api/:id', auth(1), async (req, res) => {
    try {
        const api = await API.findById(req.params.id)
        if (!api) {
            res.status(404).send({ error: 'api not found' })
            return
        }
        res.modifiedSend(api)
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.stack })
            }
    }
})

export default router
