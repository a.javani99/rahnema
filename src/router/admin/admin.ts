import axios from 'axios'
import express from 'express'
import { UploadedFile } from 'express-fileupload'
import { auth } from '../../middleware/authentication'
import Admin from '../../model/admin/admin'
import { findModel, genQueryObjs, query, saveFile } from '../../utilities'
const router = express.Router()

const billingurl = process.env.BILLING_URL
const authToken = process.env.BILLING_AUTH_TOKEN

router.post('/admin/login', async (req, res) => {
    try {
        const admin = await Admin.findByCredentials(req.body.username, req.body.password)
        const token = await admin.generateToken()
        res.cookie('token', token)
        res.modifiedSend({ admin, token })
    } catch (e) {
        res.status(401).send({ error: 'wrong username or password' })
    }
})

router.get('/admin/me', auth(1), async (req, res) => {
    res.modifiedSend(req.admin)
})

router.get('/admin', auth(1), async (req, res) => {
    res.modifiedSend(await Admin.find())
})

router.get('/admin/*', auth(1), async (req, res) => {
    const route = req.url.substring(7).split('/')
    if (route.length == 1 || route.length == 2) {
        const model = findModel(route[0].split('?')[0])
        if (model) {
            const Model = model.Model
            const populations: any = []
            const paths = Model.schema.paths
            for (let key in paths) {
                if (paths[key].schema) {
                    for (let innerKey in paths[key].schema.paths) {
                        populations.push(key + '.' + innerKey)
                    }
                } else {
                    populations.push(key)
                }
            }
            if (route.length == 1) {
                const options = genQueryObjs(req, Model)
                options.where.deleted = false
                const total = await Model.count(options.where)
                const prepResult = await query(Model, populations, options)
                const result = []
                for (let row of prepResult) {
                    result.push(row.toObject())
                }
                return res.send({ result: prepResult, total })
            } else {
                const id = route[1]
                let promise = Model.findById(id)
                for (let key of populations) {
                    promise.populate(key)
                }
                try {
                    const result = await promise
                    if (result.deleted) return res.status(404).send({ error: 'not found' })
                    return res.send(result)
                } catch (e) {
                    return res.status(404).send({ error: 'not found' })
                }
            }
        }
    }
    res.send(route)
})

router.patch('/admin/*', auth(1), async (req, res) => {
    const route = req.url.substring(7).split('/')
    if (route.length == 2) {
        const model = findModel(route[0])
        if (model) {
            const Model = model.Model
            const populations: any = []
            const paths = Model.schema.paths
            for (let key in paths) {
                if (paths[key].schema) {
                    for (let innerKey in paths[key].schema.paths) {
                        populations.push(key + '.' + innerKey)
                    }
                } else {
                    populations.push(key)
                }
            }
            const id = route[1]
            let promise = Model.findById(id)
            for (let key of populations) {
                promise.populate(key)
            }
            try {
                const result = await promise
                result.set(req.body)
                if (req.files) {
                    for (let key in req.files) {
                        if (Object.keys(paths).includes(key)) {
                            const filePath = await saveFile(req.files[key] as UploadedFile, 'public/files')
                            result[key] = filePath
                        }
                    }
                }
                await result.save()
                return res.send(result)
            } catch (e) {
                return res.status(404).send({ error: 'not found' })
            }
        }
    }
    res.send(route)
})

router.delete('/admin/*', auth(1), async (req, res) => {
    const route = req.url.substring(7).split('/')
    if (route.length == 2) {
        const model = findModel(route[0])
        if (model) {
            const Model = model.Model
            const id = route[1]
            let promise = Model.findById(id)
            try {
                const result = await promise
                result.deleted = true
                await result.save()
                return res.send(result)
            } catch (e) {
                return res.status(404).send({ error: 'not found' })
            }
        }
    }
    res.send(route)
})

router.post('/admin/*', auth(1), async (req, res) => {
    const route = req.url.substring(7).split('/')
    const model = findModel(route[0])
    if (route.length == 1) {
        if (model) {
            const Model = model.Model
            const populations: any = []
            const paths = Model.schema.paths
            for (let key in paths) {
                if (paths[key].schema) {
                    for (let innerKey in paths[key].schema.paths) {
                        populations.push(key + '.' + innerKey)
                    }
                } else {
                    populations.push(key)
                }
            }
            try {
                const newOne = await Model.create(req.body)
                let promise = Model.findById(newOne.id)
                for (let key of populations) {
                    promise.populate(key)
                }
                try {
                    const result = await promise
                    return res.send(result)
                } catch (e) {
                    return res.status(404).send({ error: 'not found' })
                }
            } catch (e: any) {
                res.status(400).send({ error: e.message })
            }
        }
    }
    res.send(route)
})
router.get('/billing/*', auth(1), async (req, res) => {
    try {
        const url = billingurl + '/' + req.url.substring(16)
        console.log('\n\nurl: ' + url)
        console.log('method: get')
        const data = (await axios.get(url, { headers: { Authorization: `${authToken}` } })).data
        console.log('success: ' + JSON.stringify(data))
        res.send(data)
    } catch (e: any) {
        if (e.response && e.response.data) {
            console.log('fail: ')
            console.log(JSON.stringify(e.response.data))
        }
        res.status(400).send({ error: e.message })
    }
})

router.post('/billing/*', auth(1), async (req, res) => {
    try {
        const url = billingurl + '/' + req.url.substring(16)
        console.log('\n\nurl: ' + url)
        console.log('method: post')
        console.log('body: ' + JSON.stringify(req.body))
        const data = (await axios.post(url, req.body, { headers: { Authorization: `${authToken}` } })).data
        res.send(data)
        console.log('success: ' + JSON.stringify(data))
    } catch (e: any) {
        if (e.response && e.response.data) {
            console.log('fail: ')
            console.log(JSON.stringify(e.response.data))
        }
        res.status(400).send({ error: e.message })
    }
})

router.patch('/billing/*', auth(1), async (req, res) => {
    try {
        const url = billingurl + '/' + req.url.substring(16)
        console.log('\n\nurl: ' + url)
        console.log('method: patch')
        console.log('body: ' + JSON.stringify(req.body))
        const data = (await axios.patch(url, req.body, { headers: { Authorization: `${authToken}` } })).data
        res.send(data)
        console.log('success: ' + JSON.stringify(data))
    } catch (e: any) {
        if (e.response && e.response.data) {
            console.log('fail: ')
            console.log(JSON.stringify(e.response.data))
        }
        res.status(400).send({ error: e.message })
    }
})

router.delete('/billing/*', auth(1), async (req, res) => {
    try {
        const url = billingurl + '/' + req.url.substring(16)
        console.log('\n\nurl: ' + url)
        console.log('method: delete')
        const data = (await axios.delete(url, { headers: { Authorization: `${authToken}` } })).data
        res.send(data)
        console.log('success: ' + JSON.stringify(data))
    } catch (e: any) {
        if (e.response && e.response.data) {
            console.log('fail: ')
            console.log(JSON.stringify(e.response.data))
        }
        res.status(400).send({ error: e.message })
    }
})
export default router
