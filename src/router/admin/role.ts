import express from 'express'
import { auth } from '../../middleware/authentication'
import Admin from '../../model/admin/admin'
import API, { IAPI } from '../../model/admin/api'
import Role, { IRole } from '../../model/admin/role'
const router = express.Router()

// valid inputs: name
// required inputs: name
router.post('/role', auth(1), async (req, res) => {
    try {
        const role = await Role.create(req.body)
        res.modifiedSend(role)
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

router.get('/role', auth(1), async (req, res) => {
    res.modifiedSend((await Role.find().select('-apis._id')).filter((s) => s.name !== 'GOD'))
})

router.get('/role/:id', auth(1), async (req, res) => {
    try {
        const role = await Role.findById(req.params.id).select('-apis._id')
        if (!role) {
            res.status(404).send({ error: 'role not found' })
            return
        }
        res.modifiedSend(role)
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

router.delete('/role/:id', auth(1), async (req, res) => {
    try {
        const role = await Role.findById(req.params.id)
        if (!role) {
            res.status(404).send({ error: 'role not found' })
            return
        }
        if (role.admins && role.admins.length > 0) {
            for (let i = 0; i < role.admins.length; i++) {
                const id: string = role.admins[i].toString()
                const admin = await Admin.findById(id)
                await admin?.removeRole(role)
            }
        }
        await role.deleteOne()
        res.modifiedSend({ msg: 'role deleted' })
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

// valid inputs: name
router.patch('/role/:id', auth(1), async (req, res) => {
    try {
        const role = await Role.findById(req.params.id)
        if (!role) {
            res.status(404).send({ error: 'role not found' })
            return
        }
        role.set(req.body)
        await role.save()
        res.modifiedSend(role)
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

router.get('/role/:id/api', auth(1), async (req, res) => {
    try {
        const role = await Role.findById(req.params.id)
        if (!role) {
            res.status(404).send({ error: 'role not found' })
            return
        }
        const apis = await role.getAPIs()
        res.modifiedSend(apis)
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

router.post('/role/:id/api/:api', auth(1), async (req, res) => {
    const role = await Role.findById(req.params.id)
    if (!role) {
        res.status(404).send({ error: 'role not found' })
        return
    }
    const api = await API.findById(req.params.api)
    if (!api) {
        res.status(404).send({ error: 'api not found' })
        return
    }
    try {
        await role.addAPI(api)
        res.modifiedSend({ msg: 'api added to role' })
    } catch (e) {
        res.modifiedSend({ msg: 'already had access' })
    }
})

router.delete('/role/:id/api/:api', auth(1), async (req, res) => {
    try {
        const role = await Role.findById(req.params.id)
        if (!role) {
            res.status(404).send({ error: 'role not found' })
            return
        }
        const api = await API.findById(req.params.api)
        if (!api) {
            res.status(404).send({ error: 'api not found' })
            return
        }
        await role.removeAPI(api)
        res.modifiedSend({ msg: 'api removed' })
    } catch (e) {
        if (e instanceof Error)
            if (e instanceof Error) {
                res.status(400).send({ error: e.message })
            }
    }
})

export default router
