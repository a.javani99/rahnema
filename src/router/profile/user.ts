import express from 'express'
import getRedis from '../../db/redis'
import { auth } from '../../middleware/authentication'
const redis = getRedis()
const router = express.Router()

router.get('/user/me', auth(2), async (req, res) => {
    res.modifiedSend(req.user)
})

router.patch('/user/me', auth(2), async (req, res) => {
    try {
        if (!req.user) throw new Error('server error: user not found')
        req.user.set(req.body)
        await req.user.save()
        res.send(req.user)
    } catch (e) {
        if (e instanceof Error) {
            res.status(400).send({ error: e.message })
        }
    }
})


export default router
