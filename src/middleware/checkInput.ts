import { Request, Response } from 'express'
import fs from 'fs'
import API from '../model/admin/api'

const checkInput = async (req: Request, res: Response, next: Function) => {
    let ou = req.originalUrl.replace('/api/v2/', '/')
    if (ou.endsWith('/')) ou = ou.substring(0, ou.length - 1)
    const allApis = await API.find({ method: req.method.toLowerCase() })
    let valids = allApis.filter((s) => s.route === ou)
    if (valids.length > 1) {
        res.status(400).send({ error: 'call backend about this api' })
        return
    }
    let api = null
    if (valids.length === 1) {
        api = valids[0]
    } else {
        let ouParts = ou.split('/')
        ouParts.shift()
        for (let i = 0; i < allApis.length; i++) {
            const s = allApis[i]
            const variableList = []
            const route = s.route
            const routeParts = route.split(':')
            routeParts.shift()
            for (let i = 0; i < routeParts.length; i++) {
                variableList.push(routeParts[i].split('/')[0])
            }

            if (variableList.length > 0) {
                const arr = permute(ouParts)
                for (let j = 0; j < arr.length; j++) {
                    let newRoute = route
                    for (let k = 0; k < variableList.length; k++) {
                        newRoute = newRoute.replace(':' + variableList[k], arr[j][k])
                    }
                    if (newRoute === ou) {
                        if (api && api.id !== s.id) {
                            res.status(400).send({ error: 'call backend about this api' })
                            return
                        } else {
                            api = s
                        }
                    }
                }
                permArr = []
                usedChars = []
            }
        }
    }
    if (api) {
        if (api.validInputs && api.validInputs.length > 0) {
            const filteredBody: any = {} // this is body sent by user, could be ANY thing
            for (let i = 0; i < api.validInputs.length; i++) {
                if (req.body[api.validInputs[i]]) {
                    filteredBody[api.validInputs[i]] = req.body[api.validInputs[i]]
                }
            }
            req.body = filteredBody
        }
        if (api.requiredInputs && api.requiredInputs.length > 0) {
            for (let i = 0; i < api.requiredInputs.length; i++) {
                if (!req.body[api.requiredInputs[i]]) {
                    res.status(400).send({ error: api.requiredInputs[i] + ' is missing' })
                    return
                }
            }
        }
        const keys = Object.keys(req.body)
        for (let i = 0; i < keys.length; i++) {
            const key = keys[i]
            if (key.endsWith('Id') && key.charAt(0) === key.charAt(0).toUpperCase()) {
                const exists = await checkforeign(key.substr(0, key.length - 2), req.body[key])
                if (!exists) {
                    res.status(400).send({ error: key + ' not found' })
                    return
                }
            }
        }
        if (api.foreignKeys && api.foreignKeys.length > 0) {
            for (let i = 0; i < api.foreignKeys.length; i++) {
                const key = api.foreignKeys[i].split('|')[0]
                const className = api.foreignKeys[i].split('|')[1]
                if (req.body[key]) {
                    const exists = await checkforeign(className, req.body[key])
                    if (!exists) {
                        res.status(400).send({ error: key + ' not found' })
                        return
                    }
                }
            }
        }
    }
    next()
}

const checkforeign = async (className: string, id: string) => {
    return true //TODO
    // try {
    //     const files = await listFiles([], process.cwd() + '/src/model')
    //     if (!files) return false
    //     for (const file of files) {
    //         const Model = require(file.path)
    //         if (Model.modelName == className) {
    //             let instance = null
    //             try {
    //                 instance = await Model.findById(id)
    //             } catch (e) { }
    //             if (!instance) {
    //                 try {
    //                     instance = await Model.find2(id)
    //                 } catch (e) { }
    //             }
    //             if (!instance) {
    //                 return false
    //             } else {
    //                 return true
    //             }
    //         }
    //     }
    // } catch (e) {
    //     return false
    // }
}

let permArr: string[] = [],
    usedChars: string[] = []

function permute(input: string[]) {
    var i, ch
    for (i = 0; i < input.length; i++) {
        ch = input.splice(i, 1)[0]
        usedChars.push(ch)
        if (input.length == 0) {
            permArr.push(...usedChars.slice())
        }
        permute(input)
        input.splice(i, 0, ch)
        usedChars.pop()
    }
    return permArr
}

const listFiles = (result: { name: string; path: string }[], dir: string) => {
    try {
        if (!result) result = []
        const files = fs.readdirSync(dir)
        for (let i = 0; i < files.length; i++) {
            if (fs.lstatSync(dir + '/' + files[i]).isDirectory()) {
                const recursive = listFiles(result, dir + '/' + files[i])
                if (recursive) result = recursive
            } else {
                result.push({ name: files[i], path: dir + '/' + files[i] })
            }
        }
        return result
    } catch (e) {
        if (e instanceof Error) console.log(e.stack)
    }
}

export default checkInput
