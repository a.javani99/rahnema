import { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import Admin, { IAdmin } from '../model/admin/admin'
import API from '../model/admin/api'
import User, { IUser } from '../model/profile/user'

export const auth = (mode: 1 | 2 | 3 | 4, strict = true) => {
    return async (req: Request, res: Response, next: Function) => {
        try {
            let token = req.header('Authorization')
            if (!token) token = req.cookies.token
            if (!token && strict) {
                res.status(401).send({ error: 'Authentication Failed' })
                return
            }
            if (!token) token = ''
            token = token.replace('Bearer ', '')
            if (mode == 1) {
                const admin = await adminAuthenticateToken(token)
                if (!admin) {
                    res.status(401).send({ error: 'Authentication Failed' })
                    return
                }
                const authorized = await isAuthorized(admin, req.route.path, req.method.toLowerCase())
                if (!authorized) {
                    res.status(403).send({ error: 'Forbidden' })
                    return
                }
                req.admin = admin
                req.token = token
                next()
            }
            if (mode == 2) {
                const user = await userAuthenticateToken(token)
                if (!user) {
                    res.status(401).send({ error: 'Authentication Failed' })
                    return
                } else {
                    req.user = user
                    req.token = token
                }
                next()
            }
            if (mode == 3) {
                const user = await userAuthenticateToken(token)
                if (user) {
                    req.user = user
                    req.token = token
                    next()
                    return
                }
                const admin = await adminAuthenticateToken(token)
                if (!admin) {
                    res.status(401).send({ error: 'Authentication Failed' })
                    return
                }
                const authorized = await isAuthorized(admin, req.route.path, req.method.toLowerCase())
                if (!authorized) {
                    res.status(403).send({ error: 'Forbidden' })
                    return
                }
                req.admin = admin
                req.token = token
                next()
            }
        } catch (e) {}
    }
}

export const userAuthenticateToken = async (token: string) => {
    try {
        if (!process.env.JWT_SECRET) return null
        const decoded = jwt.verify(token, process.env.JWT_SECRET) as { id: string }
        const user = await User.findOne({ _id: decoded.id, 'sessions.token': token, deleted: false })
        if (!user) return null
        for (let i = 0; i < user.sessions.length; i++) {
            if (user.sessions[i].token == token) {
                if (Date.now() - user.sessions[i].lastTimeUsed < 24 * 86400 * 1000) {
                    user.sessions[i].lastTimeUsed = Date.now()
                    await user.save()
                    return user
                } else {
                    user.sessions = user.sessions.splice(i, 1)
                    await user.save()
                }
            }
        }
        return null
    } catch (e) {
        return null
    }
}

export const adminAuthenticateToken = async (token: string) => {
    try {
        if (!process.env.JWT_SECRET) return null
        const decoded = jwt.verify(token, process.env.JWT_SECRET) as { id: string }
        const admin = await Admin.findOne({ _id: decoded.id })
        return admin
    } catch (e) {
        return null
    }
}

export const isAuthorized = async (admin: IAdmin, route: string, method: string) => {
    const api = await API.findOne({
        route,
        method,
    })
    if (!api) {
        return false
    }
    let authorized = false

    const adminRoles = await admin.getRoles()
    for (let j = 0; j < adminRoles.length; j++) {
        if (adminRoles[j].name === 'GOD') {
            authorized = true
        }

        const apis = await adminRoles[j].getAPIs()
        for (let i = 0; i < apis.length; i++) {
            if (apis[i].route === api.route && apis[i].method === api.method) {
                authorized = true
            }
        }
    }
    return authorized
}
