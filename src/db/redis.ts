import { createClient, RedisClientType } from 'redis'

const socket: { port?: number; host?: string } = {}
if (process.env.REDIS_PORT && !isNaN(+process.env.REDIS_PORT)) {
    socket.port = +process.env.REDIS_PORT
}
if (process.env.REDIS_HOST) {
    socket.host = process.env.REDIS_HOST
}

interface IRedis {
    client: RedisClientType
    setJ(key: string, value: any): void
    getJ(key: string): Promise<any>
    delJ(key: string): void
}

let preClient: RedisClientType | null = null
let customClient: IRedis | null = null

export default function getRedis() {
    if (!preClient || !customClient) {
        console.log('creating redis client...')
        preClient = createClient({
            socket,
            password: process.env.REDIS_PASSWORD,
        }) as RedisClientType
        preClient.connect()
        preClient.on('end', (a: any) => {
            console.log('redis ended')
        })
        preClient.on('connect', (a: any) => {
            console.log('redis connected')
        })
        preClient.on('error', (e) => {})
        customClient = {
            client: preClient,
            setJ: async (key: string, value: any) => {
                if (!preClient) throw new Error('redis client could not be found')
                await preClient.set(key, JSON.stringify(value))
            },
            getJ: async (key: string) => {
                if (!preClient) throw new Error('redis client could not be found')
                const value = await preClient.get(key)
                if (!value) return null
                return JSON.parse(value)
            },
            delJ: async (key: string) => {
                if (!preClient) throw new Error('redis client could not be found')
                await preClient.del(key)
            },
        }
    }
    return customClient
}
