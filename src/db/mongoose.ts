import mongoose from 'mongoose'
import Role from '../model/admin/role.js'
import User from '../model/profile/user.js'
import { generalApply } from '../utilities.js'
console.log(`mongodb://${process.env.DB_HOST}:27017/${process.env.DB_NAME}?authSource=admin`)
const mainDB = mongoose.createConnection(`mongodb://${process.env.DB_HOST}:27017/${process.env.DB_NAME}?authSource=admin`, {
    user: process.env.DB_USER,
    pass: process.env.DB_PASSWORD,
})
export const model = <type, model>(name: string, schema: any) => {
    schema.add({
        deleted: {
            type: Boolean,
            default: false,
        },
    })
    return mainDB.model<type, model>(name, schema)
}

export default mainDB
