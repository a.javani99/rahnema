import * as dotenv from 'dotenv'
dotenv.config({ path: process.cwd() + '/.env' })
// built in libraries

import http from 'http'
// databases
import getRedis from './src/db/redis'
import checkInput from './src/middleware/checkInput'
import Constant from './src/model/maintenance/constant.js'
import scan from './src/scanApis'
process.on('uncaughtException', function (err) {
    console.error(err.stack)
})
const redis = getRedis()

// express and socket io
import cors from 'cors'
import express from 'express'
import fileUpload from 'express-fileupload'
import mongoose from 'mongoose'
const ObjectId = mongoose.Types.ObjectId

import { listFiles } from './src/utilities'
const app = express()
const server = http.createServer(app)

function replaceValue(item: any) {
    let temp = JSON.stringify(item)
    while (temp.includes('createdAt":"')) {
        const stringDate = temp.split('createdAt":"')[1].split('"')[0]
        const numberDate = new Date(stringDate).getTime()
        temp = temp.replace('createdAt":"' + stringDate + '"', 'createdAt":' + numberDate + '')
    }
    while (temp.includes('updatedAt":"')) {
        const stringDate = temp.split('updatedAt":"')[1].split('"')[0]
        const numberDate = new Date(stringDate).getTime()
        temp = temp.replace('updatedAt":"' + stringDate + '"', 'updatedAt":' + numberDate + '')
    }
    return JSON.parse(temp)
}

app.use(function (req, res, next) {
    var send = res.send
    res.modifiedSend = function (body: any) {
        try {
            let s = JSON.stringify(body)
            s = s.replace(/"_id":/g, '"id":')
            body = JSON.parse(s)
            body = replaceValue(body)
        } catch (e) {}
        send.call(this, body)
    }
    next()
})

app.use(cors())
app.use(express.static(process.cwd() + '/public'))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.text())
app.use(fileUpload())
app.use(checkInput)
app.use((req, res, next) => {
    if (req.body.tags && typeof req.body.tags == 'string') req.body.tags = req.body.tags.split(',')
    next()
})

const loadRouters = async () => {
    const files = listFiles([], process.cwd() + '/dist/src/router')
    if (!files) return
    for (let file of files) {
        try {
            if (!file.path.endsWith('.map')) {
                const router = require(file.path)
                app.use('/api/v2', router.default)
            }
        } catch (e: any) {
            console.log(e.stack)
            console.log('error importing router: ' + file.name)
        }
    }
}
loadRouters()


scan()

const loadConstants = async () => {
    const constants = await Constant.find()
    for (let constant of constants) {
        if (constant.values && constant.values.length == 1) process.env[constant.key] = constant.values[0]
    }
    if (!process.env.INTERVAL) process.env.INTERVAL = '1000'
}

loadConstants()

server.listen(process.env.PORT, () => {
    console.log('App is listening on port ' + process.env.PORT)
})

const run = async () => {
    // before project
}
run()
